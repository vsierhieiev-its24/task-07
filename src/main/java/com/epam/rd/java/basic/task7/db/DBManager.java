package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static DBManager instance;
    public static final String setTeamsFor = "INSERT INTO users_teams(user_id, team_id) values (?, ?)";
    public static final String INSERT_INTO_USERS_LOGIN_VALUES = "INSERT INTO users(login) values(?) ";
    public static final String SELECT_ALL_USER = "SELECT * FROM users";
    public static final String SELECT_ALL_TEAM = "SELECT * FROM teams";
    public static final String SELECT_USER_BY_ID = SELECT_ALL_USER + " WHERE login= ?";
    public static final String INSERT_INTO_TEAM = "INSERT INTO teams(name) values(?)";
    public static final String SELECT_TEAM_BY_NAME = SELECT_ALL_TEAM + " WHERE name= ?";
    public static final String SELECT_TEAM_BY_ID = SELECT_ALL_TEAM + " WHERE id = ?";
    public static final String SELECT_TEAMS_BY_USER = "SELECT team_id FROM users_teams WHERE user_id = ?";
    public static final String DELETE_TEAM_BY_ID = "DELETE FROM teams WHERE name = ?";
    public static final String DELETE_USER_BY_ID = "DELETE FROM users WHERE id = ?";
    public static final String Update_TeamName_By_ID = "UPDATE teams SET name = ? WHERE id = ?";

    public static synchronized DBManager getInstance() {
        if (instance == null)
            instance = new DBManager();

        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try (var con = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
             var prepareStatement = con.prepareStatement(SELECT_ALL_USER)) {
            ResultSet resultSet = prepareStatement.executeQuery();
            while (resultSet.next()) {
                users.add(getUser(resultSet));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        try (var con = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
             var prepareStatement = con.prepareStatement(INSERT_INTO_USERS_LOGIN_VALUES, Statement.RETURN_GENERATED_KEYS)) {
            prepareStatement.setString(1, user.getLogin());
            int i = prepareStatement.executeUpdate();
            if (i != 0) return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    public boolean deleteUsers(User... users) throws DBException {
        for (int i = 0; i < users.length; i++) {
            deleteUser(users[i]);
            return true;
        }
        return false;
    }

    private void deleteUser(User user) {
        try (var con = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
             var prepareDeleteUserStatement = con.prepareStatement(DELETE_USER_BY_ID)) {
            prepareDeleteUserStatement.setInt(1, user.getId());
            prepareDeleteUserStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public User getUser(String login) throws DBException {
        try (var con = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
             var prepareStatement = con.prepareStatement(SELECT_USER_BY_ID)) {
            prepareStatement.setString(1, login);
            ResultSet resultSet = prepareStatement.executeQuery();
            if (resultSet.next()) {
                return getUser(resultSet);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    private User getUser(ResultSet resultSet) throws SQLException {
        return new User(resultSet.getInt("id"), resultSet.getString("login"));
    }

    public Team getTeam(int name) throws DBException {
        try (var con = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
             var prepareStatement = con.prepareStatement(SELECT_TEAM_BY_ID)) {
            prepareStatement.setInt(1, name);
            ResultSet resultSet = prepareStatement.executeQuery();
            if (resultSet.next()) {
                return getTeam(resultSet);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public Team getTeam(String name) throws DBException {
        try (var con = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
             var prepareStatement = con.prepareStatement(SELECT_TEAM_BY_NAME)) {
            prepareStatement.setString(1, name);
            ResultSet resultSet = prepareStatement.executeQuery();
            if (resultSet.next()) {
                return getTeam(resultSet);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    private Team getTeam(ResultSet resultSet) throws SQLException {
        return new Team(resultSet.getInt("id"), resultSet.getString("name"));
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        try (var con = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
             var prepareStatement = con.prepareStatement(SELECT_ALL_TEAM)) {
            ResultSet resultSet = prepareStatement.executeQuery();
            while (resultSet.next()) {
                teams.add(getTeam(resultSet));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        try (var con = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
             var prepareStatement = con.prepareStatement(INSERT_INTO_TEAM, Statement.RETURN_GENERATED_KEYS)) {
            prepareStatement.setString(1, team.getName());
            int i = prepareStatement.executeUpdate();
            if (i != 0) return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }
public static int idUsers = 1;
public static int teamId = 1;
    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection con = null;
        PreparedStatement prepareStatement = null;
        try {
            con = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
            prepareStatement = con.prepareStatement(setTeamsFor);
            con.setAutoCommit(false);
            for (int i = 0; i < teams.length; i++) {
                prepareStatement.setInt(1, user.getId());
                prepareStatement.setInt(2, teams[i].getId());
                int j = prepareStatement.executeUpdate();
            }
            con.commit();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            if (con != null) {
                try {
                    con.rollback();
                    throw new DBException("Thrown exception musb be a DBException", throwables);
                } catch (SQLException e) {
                    throw new DBException("Thrown exception musb be a DBException", e);
                }
            }
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (prepareStatement != null) {
                try {
                    prepareStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        try (var con = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
             var prepareStatement = con.prepareStatement(SELECT_TEAMS_BY_USER)) {
            prepareStatement.setInt(1, user.getId());
            ResultSet resultSet = prepareStatement.executeQuery();
            System.out.println(resultSet);
            List<Integer> integers = new ArrayList<>();
            while (resultSet.next()) {
                integers.add(resultSet.getInt("team_id"));
            }
            List<Team> teams = new ArrayList<>();
            for (var a : integers) {
                teams.add(getTeam(a));
            }
            return teams;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (var con = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
             var prepareDeleteTeamStatement = con.prepareStatement(DELETE_TEAM_BY_ID)) {
            prepareDeleteTeamStatement.setString(1, team.getName());
            if (prepareDeleteTeamStatement.executeUpdate() > 0)
                return true;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    public boolean updateTeam(Team team) throws DBException {
        try (var con = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
             var prepareUpdateNameStatement = con.prepareStatement(Update_TeamName_By_ID)) {
            prepareUpdateNameStatement.setString(1, team.getName());
            prepareUpdateNameStatement.setInt(2, team.getId());
            if (prepareUpdateNameStatement.executeUpdate() > 0) return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

}
