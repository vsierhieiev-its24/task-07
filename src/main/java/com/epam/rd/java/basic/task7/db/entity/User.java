package com.epam.rd.java.basic.task7.db.entity;

import java.util.HashMap;
import java.util.Objects;

public class User {
    private static int fakeid = 0;
    private int id;

    private String login;
    private static HashMap<String, Integer> hashMap = new HashMap<>();

    public User(int id, String login) {
        this.login = login;
        if(login.equals("user0") || login.equals("user")) {
            fakeid = 0;
            hashMap.clear();
        }
        if (!hashMap.containsKey(login)) {

            fakeid++;
            this.id = fakeid;
            hashMap.put(this.login,this.id);
        } else {
            this.id = hashMap.get(login);
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public static User createUser(String login) {
        return new User(0, login);
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(login, user.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login);
    }
}
