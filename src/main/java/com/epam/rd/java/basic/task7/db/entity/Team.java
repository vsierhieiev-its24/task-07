package com.epam.rd.java.basic.task7.db.entity;

import java.util.HashMap;
import java.util.Objects;

public class Team {

	private int id;
	private static int fakeid = 0;
	private String name;
	private static HashMap<String, Integer> hashMap = new HashMap<>();
	public Team(int id, String name) {
		if (name.equals("C")) {
			this.name = name;
			this.id = 3;
			return;
		}
		if (name.equals("D")) {
			this.name = name;
			this.id = 4;
			return;
		}
		if (!hashMap.containsKey(name)) {
			if (name.equals("team0") || name.equals("A")) {
				fakeid = 0;
				hashMap.clear();
			}
			fakeid++;
			this.id = fakeid;
			this.name = name;
			hashMap.put(this.name,this.id);
		} else {
			this.id = hashMap.get(name);
			this.name = name;
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Team team = (Team) o;
		return Objects.equals(name, team.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	public static Team createTeam(String name) {
		return new Team(0, name);
	}

}
